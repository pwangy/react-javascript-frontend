# VNTRS React coding challenge
This is where I'm keeping files for VNTRS' code challenge.

The objective
  1. Recreate a Careers page as defined in the spec.
  2. Fetch career data from API to populate the page.
  3. Choose at least 2 goals from the subtask list to enhance the page.
  4. Turn in the challenge 24 hrs before the next interview.

### Subtasks
- Set up dynamic routing and link the job's to separate pages where you display the job data.
- Make the page look great on all screen sizes (we all love responsive webpages)
- Add some custom styling to impress us with CSS (or preprocessor) knowledge
- Add an icon button to toggle the view between a grid list in the initial design and an image slider (the design of this one is up to your imagination)
- Add a pop-up that opens when the user clicks on the "Help" button in the bottom right corner. The Pop-up should include a form with email and text input where a user can send us a message (you don't need to send any requests but try to set it up the way that that would be as easy to add as possible)

## Approach
I started off with an ambitious goal of tackling three of the subtasks listed (responsive web, dynamic routing, and modal/pop-up) but ended up with only 1.5 done. 

Here's how I approached this challenge: Once I forked the starter code, had my npms in order, and an outline of the site built, I then tried to make the API fetch work. It worked in my browser and in postman but when trying to access the date within the API through code I continued to get an error regarding CORS. After poking around online and trying to set up a proxy I ended up reaching out for help. In order to continue on and not let this piece hold everything else up I decided to copy the information from the API to house in my production files so that the data could still be used to populate the site. After mapping the list of jobs to be listed on the Career site, then came the styling for responsiveness with a mobile-first approach. With time running short I started in on the dynamic routes but wasn't able to finish in time. I decided to freeze my progress and push what work I had in order to submit something for VNTRS to review.

As an aside: GitLab is something I was learning during this code challenge so there was a small learning curve here too.

If I had more time, I would figure out why the dynamic routing isn't quite working and then add the modal contact form. 

I hope this work becomes the basis for a productive conversation.