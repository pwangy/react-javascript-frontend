import React from 'react'
import { useParams, useHistory } from 'react-router-dom'

import jobs from '../data/jobs.json'
import { Container } from '../lib/Container'
import { Img } from '../lib/Img'

export const CareerDetail = ({ ...job }) => {
  const { jobId } = useParams()
  const history = useHistory()
  const jobMatch = jobs.find((job) => job.id === jobId)
 debugger
  console.log(jobMatch)

  if (!jobMatch) {
    history.push('/career')
  }

  return (
    <Container {...job}>
      <Img src={`${job.image}`} alt={`image for ${job.title}`} />
      <h2>{job.title}</h2>
      <p>{job.description}</p>
      <p>here are the details of the role</p>
    </Container>
  )
}