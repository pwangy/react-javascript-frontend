import React from 'react'
import { Route } from 'react-router-dom'

import { Card } from '../lib/Card'
import { TextOverlay } from '../lib/TextOverlay'
import { Overlay } from '../lib/TextOverlay'

export const Job = ({ ...job }) => {
  return (
    <div>
      <Route exact path='/Career'>
        <Card background={job.image} id={job.id}>
          <Overlay>
            <TextOverlay>{job.title}</TextOverlay>
          </Overlay>
        </Card>
      </Route>
    </div>
  )
}