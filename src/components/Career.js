/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

import jobs from '../data/jobs.json'
import { Job } from './Job'
import { Container } from '../lib/Container'

export const Career = () => {
  // const [jobs, setJobs] = useState([])
  // useEffect(() => {
  //   const listAPI = 'https://shrouded-sands-40183.herokuapp.com/https://storage.googleapis.com/ext-code-test/data.json/'
  //   // debugger

  //   fetch(listAPI)
  //     .then((res) => res.json())
  //     .then((json) => {
  //       setJobs(json.job)
  //       console.log(json)
  //     })
  // }, [])

  return (
    <div>
      <h2>Current opportunities in <span>Stockholm</span></h2>
      <Container>
        {jobs.map((job) => (
          <Link key={job.id} to={`/career/${job.jobId}`}>
            <Job {...job} />
          </Link>
        ))}
      </Container>
    </div>
  )
}